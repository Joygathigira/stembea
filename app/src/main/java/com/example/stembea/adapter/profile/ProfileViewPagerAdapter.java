package com.example.stembea.adapter.profile;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import com.example.stembea.fragment.PendingFragment;
import com.example.stembea.fragment.TripsFragment;
import com.example.stembea.fragment.UpComingFragment;

class ProfileViewPagerAdapter extends FragmentPagerAdapter {

    public ProfileViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TripsFragment tripsFragment = new TripsFragment();
                return tripsFragment;
            case 1:
                UpComingFragment upComingFragment = new UpComingFragment();
                return upComingFragment;
            case 2:
                PendingFragment pendingFragment = new PendingFragment();
                return pendingFragment;
        }
        return null;
    }

}
