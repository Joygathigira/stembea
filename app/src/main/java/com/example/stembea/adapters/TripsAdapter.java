package com.example.stembea.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.example.stembea.TripItemViewHolder;
import com.example.stembea.R;
import com.example.stembea.pojo.Trip;

import java.util.List;

public class TripsAdapter extends RecyclerView.Adapter<TripItemViewHolder> {
    Context mcContext;
    List<Trip> tripList;

    public TripsAdapter(Context mcContext, List<Trip> tripList) {
        this.mcContext = mcContext;
        this.tripList = tripList;
    }


    @Override
    public TripItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trips_item, parent, false);
        return new TripItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TripItemViewHolder holder, int position) {
        Trip trip = tripList.get(position);
        holder.bind(trip);
    }

    @Override
    public int getItemCount() {
        return tripList.size();
    }
}
