package com.example.stembea;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.stembea.pojo.Trip;

public class TripItemViewHolder extends RecyclerView.ViewHolder {
    public TextView tvDestination;
    public TextView tvDateTime;

    public TripItemViewHolder(View itemView) {
        super(itemView);
        itemView.setClickable(true);
        tvDestination = (TextView) itemView.findViewById(R.id.tvDestination);
        tvDateTime = (TextView) itemView.findViewById(R.id.tvDateTime);

    }

    public void bind(Trip trip) {
        tvDestination.setText(trip.getName());
        tvDateTime.setText(trip.getDate() );

    }

}
