package com.example.stembea.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.stembea.R;
import com.example.stembea.adapters.TripsAdapter;
import com.example.stembea.pojo.Trip;

import java.util.ArrayList;
import java.util.List;

public class UpComingFragment extends Fragment {
    RecyclerView trip_rview;
    List<Trip> list_trip;
    TripsAdapter tripsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_upcoming, container, false);
        loadUpComing();
        trip_rview = (RecyclerView) view.findViewById(R.id.recyclerViewUpcoming);

        tripsAdapter = new TripsAdapter(getActivity(), list_trip);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        trip_rview.setLayoutManager(layoutManager);
        trip_rview.setAdapter(tripsAdapter);

        return view;
    }

    void loadUpComing() {
        list_trip = new ArrayList<>();
        list_trip.add(new Trip("Kenya - Uganda", "02/04/2020"));
        list_trip.add(new Trip("Uganda - Tanzania", "09/09/2020"));
        list_trip.add(new Trip("Tanzania - Uganda", "20/05/2020"));
//        tripsAdapter.notifyDataSetChanged();

    }
}
