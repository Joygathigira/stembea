package com.example.stembea.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.stembea.R;
import com.example.stembea.adapters.TripsAdapter;
import com.example.stembea.pojo.Trip;

import java.util.ArrayList;
import java.util.List;

public class TripsFragment extends Fragment {
    RecyclerView trip_rview;
    List<Trip> list_trip;
    TripsAdapter tripsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trips, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        trip_rview = (RecyclerView) view.findViewById(R.id.recyclerViewTrips);

        loadTrip();
        tripsAdapter = new TripsAdapter(getActivity(), list_trip);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        trip_rview.setLayoutManager(layoutManager);
        trip_rview.setAdapter(tripsAdapter);
    }

    void loadTrip() {
        list_trip = new ArrayList<>();
        list_trip.add(new Trip("Tanzania", "02/04/2020"));
        list_trip.add(new Trip("Uganda", "09/09/2020"));
        list_trip.add(new Trip("Kenya", "20/05/2020"));
        list_trip.add(new Trip("Kampala", "23/03/2020"));
//        tripsAdapter.notifyDataSetChanged();

    }
}
