package com.example.stembea.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.stembea.Api_s.Api;
import com.example.stembea.R;
import com.example.stembea.models.SignUpResponse;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    SignUpResponse signUpResponsesData;
    EditText etEmail, etPassword, etName;
    Button btnsignUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        etName = (EditText) findViewById(R.id.etName);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPassword = (EditText) findViewById(R.id.etPassword);
        btnsignUp = (Button) findViewById(R.id.btnsignUp);

        findViewById(R.id.btnsignUp).setOnClickListener(this);
        findViewById(R.id.tvAlreadyhaveacc).setOnClickListener(this);


    }

    private void userSignUp(){
        String name = etName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (email.isEmpty()) {
            etEmail.setError("Email is required");
            etEmail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etEmail.setError("Enter a valid email");
            etEmail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            etPassword.setError("Password is required");
            etPassword.requestFocus();
            return;
        }

        if (password.length() < 6) {
            etPassword.setError("Password should be atleast 6 character long");
            etPassword.requestFocus();
            return;
        }

        if (name.isEmpty()) {
            etName.setError("Name is required");
            etName.requestFocus();
            return;
        }

        Call<RequestBody> call = Api
                .getInstance()
                .getApi()
                .registerClient(name, email, password);

        call.enqueue(new Callback<RequestBody>() {
            @Override
            public void onResponse(Call<RequestBody> call, Response<RequestBody> response) {

                String s = response.body().toString();
                Toast.makeText(SignUpActivity.this, s, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(Call<RequestBody> call, Throwable t) {
                Toast.makeText(SignUpActivity.this,t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnsignUp:
                userSignUp();
                break;
            case R.id.tvAlreadyhaveacc:

                startActivity(new Intent(this, SignInActivity.class));
                break;
        }
    }
}
