package com.example.stembea.activity.ui.main;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.stembea.R;
import com.example.stembea.fragment.PendingFragment;
import com.example.stembea.fragment.TripsFragment;
import com.example.stembea.fragment.UpComingFragment;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    @StringRes
    private static final int[] TAB_TITLES = new int[]{R.string.trip, R.string.upcoming, R.string.pending};
    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
//    public Fragment getItem(int position) {
//        // getItem is called to instantiate the fragment for the given page.
//        // Return a PlaceholderFragment (defined as a static inner class below).
//        return PlaceholderFragment.newInstance(position + 1);
//    }
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TripsFragment tripsFragment = new TripsFragment();
                return tripsFragment;
            case 1:
                UpComingFragment upComingFragment = new UpComingFragment();
                return upComingFragment;
            case 2:
                PendingFragment pendingFragment = new PendingFragment();
                return pendingFragment;
        }
        return null;
    }


    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getResources().getString(TAB_TITLES[position]);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 3;
    }
}