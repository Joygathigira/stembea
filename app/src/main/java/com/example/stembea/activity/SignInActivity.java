package com.example.stembea.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.stembea.Api_s.Api;
import com.example.stembea.R;
import com.example.stembea.models.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignInActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etMail;
    private EditText etPassw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        etMail = (EditText) findViewById(R.id.etMail);
        etPassw = (EditText) findViewById(R.id.etPassw);

        findViewById(R.id.btnLogin).setOnClickListener(this);
        findViewById(R.id.tvCreateAcc).setOnClickListener(this);
    }

    private void userLogin(){
        String email = etMail.getText().toString().trim();
        String password = etPassw.getText().toString().trim();

        if (email.isEmpty()) {
            etMail.setError("Email is required");
            etMail.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etMail.setError("Enter a valid email");
            etMail.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            etPassw.setError("Password is required");
            etPassw.requestFocus();
            return;
        }

        if (password.length() < 6) {
            etPassw.setError("Password should be atleast 6 character long");
            etPassw.requestFocus();
            return;
        }

        Call<LoginResponse> call = Api
                .getInstance()
                .getApi()
                .loginClient(email,password);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse loginResponse = response.body();

                if (!loginResponse.isError()){
                    Toast.makeText(SignInActivity.this,LoginResponse.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                userLogin();

                startActivity(new Intent(this, HomeActivity.class));
                break;
            case R.id.tvCreateAcc:

                startActivity(new Intent(this, SignUpActivity.class));
                break;
        }
    }
}
