package com.example.stembea.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.stembea.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

    }
}
