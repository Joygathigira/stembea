package com.example.stembea.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridLayout;
import android.widget.Toast;

import com.example.stembea.R;

public class BookYourTripActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_your_trip);
    }

    public void setSingleEvent(GridLayout maingrid) {
        for (int i = 0; i < maingrid.getChildCount(); i++) {
            final CardView cardView = (CardView) maingrid.getChildAt(i);
            final int finalI = i;

            cardView.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent event) {
                    Context mcontext = null;
                    if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                        Toast.makeText(mcontext, "Button: " + finalI, Toast.LENGTH_SHORT).show();
                        cardView.setCardBackgroundColor(mcontext.getResources().getColor(R.color.buttonpressed));
                        if (finalI == 0) {
                            mcontext.startActivity(new Intent(mcontext, UpComingTripsActivity.class));
                        }
                    } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                        /* Reset Color */
                        cardView.setCardBackgroundColor(mcontext.getResources().getColor(R.color.red));
                    }
                    return true;
                    }
                });
        }}
}
