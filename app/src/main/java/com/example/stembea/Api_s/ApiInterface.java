package com.example.stembea.Api_s;

import com.example.stembea.models.LoginResponse;

import okhttp3.RequestBody;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("register")
    Call<RequestBody> registerClient(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse > loginClient(
            @Field("email") String email,
            @Field("password") String password
    );



}
