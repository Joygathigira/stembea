package com.example.stembea.Api_s;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {

    private static final String BASE_URL = "http://15.206.157.14/api/";
    private static Api mInstance;
    private Retrofit retrofit;

    private Api(){
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized Api getInstance(){
        if (mInstance == null){
            mInstance = new Api();
        }
        return mInstance;
    }

    public ApiInterface getApi(){
        return retrofit.create(ApiInterface.class);
    }
}
